import io.scenarium.pluginManager.PluginsSupplier;
import io.scenarium.record.Plugin;

module io.scenarium.record {
	uses PluginsSupplier;

	provides PluginsSupplier with Plugin;

	requires transitive io.scenarium.gui.core;
	requires transitive io.scenarium.flow;
	requires javafx.controls;

	exports io.scenarium.record;
	exports io.scenarium.record.gui.receditor;
	exports io.scenarium.record.operator.recorder;
	exports io.scenarium.record.operator.player;
	exports io.scenarium.record.scenario;

}
