package io.scenarium.record;

import io.beanmanager.PluginsBeanSupplier;
import io.beanmanager.consumer.ClassNameRedirectionConsumer;
import io.scenarium.core.PluginsCoreSupplier;
import io.scenarium.core.consumer.ScenarioConsumer;
import io.scenarium.flow.consumer.OperatorConsumer;
import io.scenarium.flow.consumer.PluginsFlowSupplier;
import io.scenarium.gui.core.PluginsGuiCoreSupplier;
import io.scenarium.gui.core.consumer.ToolBarConsumer;
import io.scenarium.gui.core.display.ToolBarInfo;
import io.scenarium.pluginManager.PluginsSupplier;
import io.scenarium.record.gui.receditor.LogMerger;
import io.scenarium.record.operator.player.RecordPlayer;
import io.scenarium.record.operator.recorder.CSVRecorder;
import io.scenarium.record.operator.recorder.Recorder;
import io.scenarium.record.scenario.PrimitiveData;

import javafx.scene.input.KeyCode;

public class Plugin implements PluginsSupplier, PluginsFlowSupplier, PluginsBeanSupplier, PluginsGuiCoreSupplier, PluginsCoreSupplier {
	@Override
	public void populateOperators(OperatorConsumer operatorConsumer) {
		operatorConsumer.accept(Recorder.class);
		operatorConsumer.accept(CSVRecorder.class);
		operatorConsumer.accept(RecordPlayer.class);
	}

	@Override
	public void populateClassNameRedirection(ClassNameRedirectionConsumer classNameRedirectionConsumer) {
		classNameRedirectionConsumer.accept("org.scenarium.operator.record.Recorder", "io.scenarium.record.operator.recorder.Recorder");
		classNameRedirectionConsumer.accept("org.scenarium.operator.record.CSVRecorder", "io.scenarium.record.operator.recorder.CSVRecorder");
		classNameRedirectionConsumer.accept("org.scenarium.filemanager.scenario.PrimitiveData", "io.scenarium.record.scenario.PrimitiveData");
	}

	@Override
	public void populateScenarios(ScenarioConsumer scenarioConsumer) {
		scenarioConsumer.accept(PrimitiveData.class);
	}

	@Override
	public void populateToolBars(ToolBarConsumer toolBarConsumer) {
		toolBarConsumer.accept(new ToolBarInfo(LogMerger.class, KeyCode.M, true, null));
	}
}
