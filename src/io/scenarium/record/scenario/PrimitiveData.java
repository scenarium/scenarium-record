/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.record.scenario;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.nio.channels.ClosedByInterruptException;
import java.util.LinkedHashMap;
import java.util.Objects;

import io.scenarium.core.filemanager.scenariomanager.ScenarioException;
import io.scenarium.core.filemanager.scenariomanager.TimedScenario;
import io.scenarium.core.timescheduler.Scheduler;
import io.scenarium.record.internal.Log;
import io.scenarium.river.filerecorder.FileStreamManager;
import io.scenarium.river.filerecorder.reader.FileStreamReader;

public class PrimitiveData extends TimedScenario/* implements IOTypeChanger */ {
	private long nbDatas = -1;
	private Class<?> type;

	private FileStreamReader reader;

	@Override
	public boolean canCreateDefault() {
		return false;
	}

	@Override
	protected synchronized boolean close() {
		super.close();
		try {
			this.reader.close();
			return true;
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

	@Override
	public long getBeginningTime() {
		return 0;
	}

	@Override
	public Class<?> getDataType() {
		return this.type;
	}

	@Override
	public long getNbFrame() {
		if (this.nbDatas == -1 && this.file != null)
			try {
				load(this.file, false);
				close();
			} catch (IOException | ScenarioException e) {
				return -1;
			}
		return this.nbDatas;
	}

	@Override
	public String[] getReaderFormatNames() {

		return FileStreamManager.getReaderFormatNames();
	}

	@Override
	public int getSchedulerType() {
		return Scheduler.TIMER_SCHEDULER;
	}

	@Override
	public synchronized void load(File scenarioFile, boolean backgroundLoading) throws IOException, ScenarioException {
		Log.info("Load primitive Data: '" + scenarioFile + "'");
		try {
			this.type = FileStreamManager.getRecordFileClass(scenarioFile);
		} catch (IOException e) {
			Log.error("Error while loading: '" + scenarioFile + "'");
			Log.error("Caused by: " + e.getClass().getSimpleName() + ": " + e.getMessage());
		}
		Log.info("try to find the decoder of input data: '" + this.type + "'");
		if (this.type == null)
			Log.error("Can not retreive the type of an input: '" + scenarioFile + "'");
		//return;
		this.reader = FileStreamManager.getReader(this.type);
		if (this.reader == null)
			Log.error("Can not create a reader for: '" + scenarioFile + "'");
		//return;
		String absolutePath = new File(scenarioFile.getAbsolutePath()).getParent();
		this.reader.setPath(absolutePath);
		String fileName = scenarioFile.getName();
		this.reader.setName(fileName.substring(0, fileName.lastIndexOf(".")));
		this.nbDatas = this.reader.size();
		this.scenarioData = this.reader.pop();
		fireLoadChanged();
	}

	@Override
	public void populateInfo(LinkedHashMap<String, String> info) throws IOException {}

	@Override
	public synchronized void process(Long timePointer) throws IOException, ClassNotFoundException, ScenarioException {
		long frame = (long) (timePointer / getPeriod());
		if (frame >= this.nbDatas) {
			this.scenarioData = null;
			return;
		}
		try {
			this.reader.seek(frame);
			this.scenarioData = this.reader.pop();
		} catch (ClosedByInterruptException e) {
			// death();
			// load(file, false); //Pk c'est normal une ClosedByInterruptException si interruptedException...
		} catch (EOFException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void save(File file) throws IOException {
		return;
	}

	@Override
	public void setFile(File file) {
		if (Objects.equals(file, this.file))
			return;
		if (file == null) {
			super.setFile(null);
			return;
		}
		try {
			this.type = FileStreamManager.getRecordFileClass(file);
		} catch (IOException ex) {
			if (!file.toString().isEmpty())
				Log.error("Error while setting file: " + file + " caused by: " + ex.getClass().getSimpleName());
		}
		super.setFile(file);
	}
}
