package io.scenarium.record.operator.player;

import java.util.Arrays;
import java.util.stream.Collectors;

import io.scenarium.flow.operator.player.ScenarioPlayer;
import io.scenarium.flow.scenario.ScenariumScheduler;

public class RecordPlayer extends ScenarioPlayer {

	@Override
	public String[] getFilters() {
		return new String[] {"Records " + Arrays.stream(new ScenariumScheduler().getReaderFormatNames()).collect(Collectors.joining(" "))};
	}
}
