/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.record.gui.receditor;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.function.BiConsumer;

import io.beanmanager.BeanManager;
import io.beanmanager.editors.PropertyInfo;
import io.scenarium.core.Scenarium;
import io.scenarium.core.filemanager.scenariomanager.LocalScenario;
import io.scenarium.core.filemanager.scenariomanager.ScenarioException;
import io.scenarium.core.filemanager.scenariomanager.ScenarioTrigger;
import io.scenarium.core.timescheduler.EvenementScheduler;
import io.scenarium.core.timescheduler.ScheduleTask;
import io.scenarium.core.timescheduler.SchedulerInterface;
import io.scenarium.core.timescheduler.TriggerMode;
import io.scenarium.flow.scenario.ScenariumScheduler;
import io.scenarium.gui.core.display.toolbarclass.ExternalTool;
import io.scenarium.record.internal.Log;
import io.scenarium.record.operator.recorder.Recorder;

import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class LogMerger extends ExternalTool {
	@PropertyInfo(index = 0, info = "List of Scenarium log to merge")
	private File[] recToMerge;
	@PropertyInfo(index = 1, info = "Output directory of the merged log")
	private File outputDirectory;
	private final File configFile = new File(Scenarium.getWorkspace() + File.separator + getClass().getSimpleName() + ".txt");

	public File getOutputDirectory() {
		return this.outputDirectory;
	}

	public File[] getRecToMerge() {
		return this.recToMerge;
	}

	@Override
	public Region getRegion() {
		BeanManager bm = new BeanManager(this, getClass().getSimpleName());
		bm.load(this.configFile);
		Button launchButton = new Button("Merge");
		launchButton.setOnAction(e -> {
			bm.save(this.configFile, true);
			Task<Void> mergeTask = new Task<>() {
				@Override
				protected Void call() throws Exception {
					LogMerger.this.mergeRecord(this::updateProgress);
					return null;
				}
			};
			Stage progressStage = new Stage();
			ProgressBar pb = new ProgressBar();
			pb.progressProperty().bind(mergeTask.progressProperty());
			pb.setPrefSize(640, 30);
			Text progressText = new Text("Initialisation");
			Button cancelButton = new Button("Cancel");
			cancelButton.setOnAction(e1 -> mergeTask.cancel());
			progressText.textProperty().bind(mergeTask.progressProperty().multiply(100).asString("%.2f").concat("%"));
			VBox vBox = new VBox(new StackPane(pb, progressText), cancelButton);
			vBox.setAlignment(Pos.CENTER);
			progressStage.setScene(new Scene(vBox));
			progressStage.show();
			mergeTask.setOnSucceeded(e1 -> progressStage.close());
			mergeTask.setOnCancelled(e1 -> progressStage.close());
			Thread th = new Thread(mergeTask);
			th.setDaemon(true);
			th.start();

		});
		VBox propertiesBox = new VBox(10, bm.getEditor(), launchButton);
		propertiesBox.setPadding(new Insets(10));
		propertiesBox.setAlignment(Pos.CENTER);
		return propertiesBox;
	}

	private void mergeRecord(BiConsumer<Long, Long> updateProgress) {
		ArrayList<ScheduleTask> schedulableTasks = new ArrayList<>();
		String[][] scenariosNames = new String[this.recToMerge.length][];
		for (int i = 0; i < this.recToMerge.length; i++) {
			ScenariumScheduler sc = new ScenariumScheduler();
			try {
				sc.load(this.recToMerge[i], false);
				sc.setScheduler(new SchedulerInterface(new EvenementScheduler(false) {
					@Override
					public void setTasks(ArrayList<ScheduleTask> schedulableTasks) {
						schedulableTasks.addAll(schedulableTasks);
					}
				}));
				scenariosNames[i] = sc.getScenariosName();
			} catch (IOException | ScenarioException ex) {
				scenariosNames[i] = new String[0];
				ex.printStackTrace();
			}
		}
		TriggerMode triggerMode = TriggerMode.TIME_OF_ISSUE;
		Collections.sort(schedulableTasks, triggerMode == TriggerMode.TIMESTAMP ? (a, b) -> Long.compare(a.timeStamp, b.timeStamp) : (a, b) -> Long.compare(a.timeofIssue, b.timeofIssue));
		LinkedHashMap<String, Class<?>> nameTypemap = new LinkedHashMap<>();
		for (ScheduleTask st : schedulableTasks) {
			LocalScenario ls = (LocalScenario) st.task;
			String name = ScenariumScheduler.getScenarioName(ls.getFile().getName());
			Class<?> c = nameTypemap.get(name);
			if (c == null)
				nameTypemap.put(name, ((LocalScenario) st.task).getDataType());
			else if (c != ((LocalScenario) st.task).getDataType()) {
				Log.error("Conflict, there is two log with the name " + name + " be with differrent type: " + c.getSimpleName() + " and " + ((LocalScenario) st.task).getDataType());
				return;
			}
		}
		String[] names = new String[nameTypemap.size()];
		Class<?>[] types = nameTypemap.values().toArray(Class[]::new);
		next: for (int i = 0; i < scenariosNames.length; i++) {
			ArrayList<String> toRemove = new ArrayList<>();
			for (String name : nameTypemap.keySet()) {
				String[] scenarioNames = scenariosNames[i];
				for (int j = 0; j < scenarioNames.length; j++) {
					String scenarioName = scenarioNames[j];
					if (scenarioName.equals(name) && names[j] == null) {
						names[j] = name;
						types[j] = nameTypemap.get(name);
						toRemove.add(name);
						break;
					}
				}
			}
			for (String name : toRemove)
				nameTypemap.remove(name);
			for (String name : names)
				if (name == null)
					continue next;
			break;
		}
		nameTypemap.forEach((name, type) -> {
			for (int i = 0; i < names.length; i++)
				if (names[i] != null) {
					names[i] = name;
					types[i] = type;
					return;
				}
		});
		class TimeManagedRecorder extends Recorder {
			private long ts;
			private long toi;

			@Override
			public String[] getOutputLinkToInputNames() {
				String[] outputLinkToInputName = new String[names.length];
				for (int i = 0; i < names.length; i++) {
					String name = names[i];
					int index;
					if ((index = name.indexOf(Recorder.RECORD_INDEX_SEPARATOR)) != -1)
						try {
							Integer.parseInt(name.substring(0, index));
							outputLinkToInputName[i] = name.substring(index + 1);
							continue;
						} catch (NumberFormatException e) {}
					outputLinkToInputName[i] = name;
				}
				return outputLinkToInputName;
			}

			@Override
			public Class<?>[] getOutputLinkToInputTypes() {
				return types;
			}

			@Override
			public long getTimeOfIssue(int i) {
				return this.toi;
			}

			@Override
			public long getTimeStamp(int i) {
				return this.ts;
			}

			public void setTimeOfIssue(long toi) {
				this.toi = toi;
			}

			public void setTimeStamp(long ts) {
				this.ts = ts;
			}
		}
		TimeManagedRecorder recorder = new TimeManagedRecorder();
		recorder.setRecordDirectory(this.outputDirectory);
		class IndexedTrigger implements ScenarioTrigger {
			Object[] recordObjects = new Object[names.length];
			private int indexOfRecord;

			public void setIndex(int index) {
				this.indexOfRecord = index;
			}

			@Override
			public boolean isAlive() {
				return true;
			}

			@Override
			public boolean isActive() {
				return true;
			}

			@Override
			public Object[] generateOuputsVector() {
				return null;
			}

			@Override
			public void setWarning(String warning) {}

			@Override
			public boolean holdLock() {
				return true;
			}

			@Override
			public String getName() {
				return "";
			}

			@Override
			public void runLater(Runnable task) {
				task.run();
			}

			@Override
			public Object[] getAdditionalInputs() {
				return null;
			}

			@Override
			public long getTimeStamp(int i) {
				return -1;
			}

			@Override
			public boolean triggerOutput(Object[] ouputsVector, long[] timeStamps) {
				Arrays.fill(this.recordObjects, null);
				this.recordObjects[this.indexOfRecord] = ouputsVector[0];
				recorder.process(this.recordObjects);
				return true;
			}

			@Override
			public boolean triggerOutput(Object[] objects, long timeStamp) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean triggerOutput(Object outputValue, long timeStamp) {
				// TODO Auto-generated method stub
				return false;
			}
		}
		IndexedTrigger trigger = new IndexedTrigger();
		HashMap<LocalScenario, Integer> indexOfLocalScenario = new HashMap<>();
		for (ScheduleTask st : schedulableTasks) {
			LocalScenario ls = (LocalScenario) st.task;
			if (indexOfLocalScenario.get(ls) == null) {
				String name = ScenariumScheduler.getScenarioName(ls.getFile().getName());
				for (int j = 0; j < names.length; j++)
					if (name.equals(names[j])) {
						indexOfLocalScenario.put(ls, j);
						break;
					}
				ls.setTrigger(trigger);
			}
		}
		recorder.birth();
		int index = 0;
		for (ScheduleTask st : schedulableTasks) {
			LocalScenario ls = (LocalScenario) st.task;
			trigger.setIndex(indexOfLocalScenario.get(ls));
			recorder.setTimeOfIssue(st.timeofIssue);
			recorder.setTimeStamp(st.timeStamp);
			ls.update(st.seekIndex);
			updateProgress.accept((long) index++, (long) schedulableTasks.size());
			if (Thread.currentThread().isInterrupted())
				break;
		}
		recorder.death();
		if (Thread.currentThread().isInterrupted())
			try {
				Files.walk(Paths.get(recorder.getRecordedPath()), FileVisitOption.FOLLOW_LINKS).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
			} catch (IOException e) {
				e.printStackTrace();
			}
		updateProgress.accept(1L, 1L);
	}

	public void setOutputDirectory(File outputDirectory) {
		this.outputDirectory = outputDirectory;
	}

	public void setRecToMerge(File[] recToMerge) {
		this.recToMerge = recToMerge;
	}
}
