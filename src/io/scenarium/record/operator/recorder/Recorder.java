/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.record.operator.recorder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import io.beanmanager.editors.PropertyInfo;
import io.scenarium.core.editors.NotChangeableAtRuntime;
import io.scenarium.flow.AbstractRecorder;
import io.scenarium.flow.ParamInfo;
import io.scenarium.flow.VarArgsInputChangeListener;
import io.scenarium.flow.operator.EvolvedVarArgsOperator;
import io.scenarium.record.internal.Log;
import io.scenarium.river.filerecorder.FileStreamManager;
import io.scenarium.river.filerecorder.recorder.FileStreamRecorder;

public class Recorder extends AbstractRecorder implements VarArgsInputChangeListener, EvolvedVarArgsOperator {
	public static final String RECORD_INDEX_SEPARATOR = "_";
	private String[] outputLinkToInputName;
	private Class<?>[] outputLinkToInputType;
	private FileStreamRecorder[] streamRecorders;
	private RandomAccessFile recFos; // Le passer en dataoutputstream
	private String recordedPath;
	private int[] recordId;
	private int recordIdCpt = 0;
	private File recFile;
	private int nbDataRecorded;
	@PropertyInfo(index = 10, info = "Store the scheduling file in text format, use more space but can be read and modify easily")
	@NotChangeableAtRuntime
	private boolean textMode = false;

	private int recordPt;
	private boolean isWarning;

	// TODO sceptique..., ca marche en compact?
	private void addToHeader(int indexOfInput) {
		try {
			this.recFos.close();
			File tempFile = new File(this.recFile.getAbsolutePath() + "-");
			this.recFile.renameTo(tempFile);
			this.recFos = new RandomAccessFile(this.recFile, "rw");
			try (RandomAccessFile rtemp = new RandomAccessFile(tempFile, "rw")) {
				this.recFos.writeByte(rtemp.readByte());
				if (this.textMode) {
					int nbInput = Integer.parseInt(rtemp.readLine());
					this.recFos.writeBytes(Integer.toString(nbInput + 1));
					for (int i = 0; i < nbInput; i++)
						this.recFos.writeBytes(System.lineSeparator() + rtemp.readLine());
					this.recFos.writeBytes(System.lineSeparator() + this.streamRecorders[indexOfInput].getName() + "." + this.streamRecorders[indexOfInput].getExtention() + ":"
							+ this.recordId[indexOfInput] + System.lineSeparator());
				} else {
					int nbInput = rtemp.readInt();
					this.recFos.writeInt(nbInput + 1);
					for (int i = 0; i < nbInput; i++)
						putString(this.recFos, getString(rtemp));
					putString(this.recFos, this.streamRecorders[indexOfInput].getName() + "." + this.streamRecorders[indexOfInput].getExtention());
				}
				long offset = rtemp.getFilePointer();
				rtemp.getChannel().transferTo(offset, rtemp.length() - offset, this.recFos.getChannel());
			}
			tempFile.delete();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void birth() {
		this.isWarning = false;
		if (!isRecording())
			return;
		this.nbDataRecorded = 0;
		this.recordIdCpt = 0;
		this.outputLinkToInputName = getOutputLinkToInputNames();
		HashMap<String, ArrayList<Integer>> similar = new HashMap<>();
		for (int i = 0; i < this.outputLinkToInputName.length; i++) {
			String name = this.outputLinkToInputName[i];
			if (name != null) {
				ArrayList<Integer> groupName = similar.get(name);
				if (groupName == null) {
					groupName = new ArrayList<>();
					similar.put(name, groupName);
				}
				groupName.add(i);
			}
		}
		for (ArrayList<Integer> groupName : similar.values())
			if (groupName.size() != 1) {
				int i = 0;
				for (Integer id : groupName)
					this.outputLinkToInputName[id] += "-" + i++;
			}
		this.outputLinkToInputType = getOutputLinkToInputTypes();
		int nbRecorder = 0;
		for (Class<?> type : this.outputLinkToInputType)
			if (type != null)
				nbRecorder++;
		this.streamRecorders = new FileStreamRecorder[nbRecorder];
		this.recordId = new int[nbRecorder];
		this.recordPt = 0;
		String recordName = "Rec_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(System.currentTimeMillis());
		this.recordedPath = getRecordDirectory() + File.separator + recordName;
		int nbStreamRecorder = 0;
		for (int i = 0; i < this.streamRecorders.length; i++) {
			FileStreamRecorder sr = FileStreamManager.getRecorder(this.outputLinkToInputType[i]);
			if (sr == null)
				continue;
			nbStreamRecorder++;
			sr.setPath(this.recordedPath);
			sr.setName(this.recordPt++ + RECORD_INDEX_SEPARATOR + this.outputLinkToInputName[i]);
			this.streamRecorders[i] = sr;
			this.recordId[i] = this.recordIdCpt++;
		}
		try {
			this.recFile = new File(this.recordedPath + File.separator + recordName + ".psf");
			this.recFile.getParentFile().mkdirs();
			this.recFos = new RandomAccessFile(this.recFile, "rw");
			// TODO Indice de version, version compacte en 1
			if (this.textMode) {
				this.recFos.writeByte(0);
				this.recFos.writeBytes(Integer.toString(nbStreamRecorder));
				for (int i = 0; i < this.streamRecorders.length; i++)
					if (this.streamRecorders[i] != null)
						this.recFos.writeBytes(System.lineSeparator() + this.streamRecorders[i].getName() + "." + this.streamRecorders[i].getExtention() + ":" + this.recordId[i]);
			} else {
				this.recFos.writeByte(1);
				this.recFos.writeInt(nbStreamRecorder);
				for (int i = 0; i < this.streamRecorders.length; i++)
					if (this.streamRecorders[i] != null)
						putString(this.recFos, this.streamRecorders[i].getName() + "." + this.streamRecorders[i].getExtention());
			}
		} catch (IOException e) {
			if (e instanceof FileNotFoundException)
				Log.error("The file: " + new File(this.recordedPath).getParent() + " does not exist");
			else
				e.printStackTrace();
			setError(e.getMessage());
			return;
		}
		addVarArgsInputChangeListener(this);
	}

	@Override
	public boolean canAddInput(Class<?>[] inputsType) {
		return true;
	}

	@Override
	public void death() {
		if (this.recFos != null) {
			try {
				this.recFos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			this.recFos = null;
		}
		if (this.streamRecorders != null) {
			for (FileStreamRecorder sr : this.streamRecorders)
				if (sr != null)
					try {
						sr.close();
					} catch (IOException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
			this.streamRecorders = null;
		}
		removeVarArgsInputChangeListener(this);
	}

	public String getRecordedPath() {
		return this.recordedPath;
	}

	public boolean isTextMode() {
		return this.textMode;
	}

	@Override
	public boolean isValidInput(Class<?>[] inputsType, Class<?> additionalInput) {
		return additionalInput != null && FileStreamManager.hasStreamRecorder(additionalInput);
	}

	@ParamInfo(in = "r", out="recordCount")
	public Integer process(final Object... objs) {
		if (this.recFos == null) {
			if (isRecording() && !this.isWarning) {
				setWarning("Unable to record probably due to some errors during birth");
				this.isWarning = true;
			}
			return null;
		} else if (this.isWarning) {
			setWarning(null);
			this.isWarning = false;
		}
		for (int i = 0; i < objs.length; i++) {
			Object obj = objs[i];
			if (obj != null) {
				FileStreamRecorder sr = this.streamRecorders[i];
				if (sr == null)
					continue;
				try {
					this.nbDataRecorded++;
					if (this.textMode)
						this.recFos.writeBytes(System.lineSeparator() + getTimeOfIssue(i) + ":" + getTimeStamp(i) + ":" + this.recordId[i] + ":" + sr.getPosition());
					else {
						this.recFos.writeLong(getTimeOfIssue(i));
						this.recFos.writeLong(getTimeStamp(i));
						this.recFos.writeInt(this.recordId[i]);
						this.recFos.writeLong(sr.getPosition());
					}
					sr.push(obj);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return this.nbDataRecorded;
	}

	private static void putString(RandomAccessFile raf, String value) throws IOException {
		byte[] nameb = value.getBytes(StandardCharsets.UTF_8);
		raf.writeInt(nameb.length);
		raf.write(nameb);
	}

	public void setTextMode(boolean textMode) {
		this.textMode = textMode;
	}

	@Override
	public void varArgsInputChanged(int indexOfInput, int type) {
		this.outputLinkToInputName = getOutputLinkToInputNames();
		this.outputLinkToInputType = getOutputLinkToInputTypes();
		if (type == VarArgsInputChangeListener.CHANGED) {
			try {
				this.streamRecorders[indexOfInput].close();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			this.streamRecorders[indexOfInput] = FileStreamManager.getRecorder(this.outputLinkToInputType[indexOfInput]);
			this.streamRecorders[indexOfInput].setPath(this.recordedPath);
			this.streamRecorders[indexOfInput].setName(this.recordPt++ + RECORD_INDEX_SEPARATOR + this.outputLinkToInputName[indexOfInput]);
			this.recordId[indexOfInput] = this.recordIdCpt++;
			addToHeader(indexOfInput);
		} else if (type == VarArgsInputChangeListener.NEW) {
			FileStreamRecorder[] newStreamRecorders = new FileStreamRecorder[indexOfInput + 1];
			System.arraycopy(this.streamRecorders, 0, newStreamRecorders, 0, this.streamRecorders.length);
			int indexOfNewInput = newStreamRecorders.length - 1;
			int[] newRecordId = new int[newStreamRecorders.length];
			System.arraycopy(this.recordId, 0, newRecordId, 0, this.recordId.length);
			newStreamRecorders[indexOfNewInput] = FileStreamManager.getRecorder(this.outputLinkToInputType[indexOfNewInput]);
			newStreamRecorders[indexOfNewInput].setPath(this.recordedPath);
			newStreamRecorders[indexOfNewInput].setName(this.recordPt++ + RECORD_INDEX_SEPARATOR + this.outputLinkToInputName[indexOfNewInput]);
			newRecordId[indexOfNewInput] = this.recordIdCpt++;
			this.streamRecorders = newStreamRecorders;
			this.recordId = newRecordId;
			addToHeader(indexOfNewInput);
		} else if (type == VarArgsInputChangeListener.REMOVE) {
			try {
				this.streamRecorders[indexOfInput].close();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			FileStreamRecorder[] newStreamRecorders = new FileStreamRecorder[this.streamRecorders.length - 1];
			System.arraycopy(this.streamRecorders, 0, newStreamRecorders, 0, indexOfInput);
			int[] newRecordId = new int[newStreamRecorders.length];
			System.arraycopy(this.recordId, 0, newRecordId, 0, newRecordId.length);
			for (int i = indexOfInput + 1; i < this.streamRecorders.length; i++) {
				newStreamRecorders[i - 1] = this.streamRecorders[i];
				newRecordId[i - 1] = this.recordId[i];
			}
			this.streamRecorders = newStreamRecorders;
			this.recordId = newRecordId;
		}
	}

	private static String getString(RandomAccessFile raf) throws IOException {
		int nbBytes = raf.readInt();
		byte[] data = new byte[nbBytes];
		raf.readFully(data);
		return new String(data, StandardCharsets.UTF_8);
	}
}
